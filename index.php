<?php

// Routes
require 'routes.php';

// View partials
require 'public/views/_viewpartials.php';
?>
<html>
  <head>

      <link rel="stylesheet" type="text/css" href="public/assets/css/style.css">
  </head>
  <body>
      <?php echo $menu; ?>
      <div class="container" id="searchContainer">
        <div class="row">
          <div class="col-sm-12">
              <div class="row mt-4">
                <div class="col-lg-6 col-sm-12">
                  <h1 id="brand" class="text-black">Arrivals & Depatures</h1>
                </div>
                <div class="col-lg-6 col-sm-12  text-right mt-2">
                  <div id="typeGroup" class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" data-value="arrivals" class="btn active typeSelect btn-secondary">Arrivals</button>
                        <button type="button" data-value="depatures" class="btn typeSelect btn-secondary">Depatures</button>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="row ">
          <div class="col-sm-12">
            <form id="search" class=" bg-light d-flex justify-content-center">
              <div class="row">
                <div class="col-lg-3 col-sm-12">
                  <div class="form-group ml-2 mr-2">
                    <label for="inputCity">City</label>
                    <input class="form-control" type="text" id="inputCity" placeholder="London...">
                  </div>
                </div>
                <div class="col-lg-3 col-sm-12">
                  <div class="form-group ml-2 mr-2">
                    <label for="inputDate">Date</label>
                    <input class="form-control" type="date" id="inputDate" placeholder="4/4/2018">
                  </div>
                </div>
                <div class="col-lg-3 col-sm-12">
                    <div class="form-group ml-2 mr-2">
                      <label for="inputTime">Time</label>
                      <select class="form-control" placeholder="Time" id="inputTime" value="00">
                          <option selected="">00:00</option>
                      </select>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12">

                  <button type="submit" class="form-control" id="inputSubmit"> Søg </button>

                </div>
              </div>
            </form>
          </div>
        </div>
        <h1 id="result"></h1>
        <?php echo $content; ?>
      </div>
      <?php echo $footer; ?>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="public/assets/js/main.min.js"></script>
  </body>
</html>
