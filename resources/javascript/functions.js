$(document).ready(function() {


  // Tilføjer timer til tid select
  for(i=0;i<24;i++) {
    $('#inputTime').append('<option value="'+i+'">'+i+':00</option>');
  }

  // Henter data
  $( "#search" ).submit(function( event ) {
      event.preventDefault();
      inputTime = $('#inputTime').val();
      inputCity = $('#inputCity').val();
      inputDate = $('#inputDate').val();
      type = $('.typeSelect.active').attr('data-value');

      var query = {
        request: 'fetchAll',
        type: type,
        time: inputTime,
        city: inputCity,
        date: inputDate
      };

      fetchFlights(query);
  });
});

$( ".typeSelect").on('click', function( event ) {
    $( ".typeSelect.active").removeClass('active');
    $(this).addClass('active');
});

// Henter fly tider
function fetchFlights(query) {
  $.ajax({
    method: "GET",
    url: "api.php",
    data: query
  })
    .done(function( data ) {
      setFlights(data);
  });
}

function setFlights(data){

    // Clear past html
   $('#flights').html('');


  var obj = JSON.parse(data);



  var html = '';
  $.each(obj, function(key, value) {
      html += '<div class="row mt-5" >';
        html += '<div class="col-lg-12 flight">';
          html += '<hr>';
          html += '<h1>'+value['destination']+'</h1>';
          html += '<h3>'+value['time']+'</h3>';
          html += '<h5>Terminal: '+value['terminal']+'</h5>';
          html += '<hr>';
        html += '</div>';
      html += '</div>';
  });

  $('#flights').append(html);


}
