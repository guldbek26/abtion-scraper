# Installation

### Type the following commands




- composer install
- npm install
- npm run scss
- npm run js



#### Required composer dependencies

- Goutte/FriendsOfPHP

#### Required npm packages

- uglify-js
- node-sass
