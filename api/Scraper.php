<?php

class Scraper {

      public $data;
      private $client;

      public function __construct($client) {
          $this->client = $client;
      }


      public function fetchAll($type, $city, $date, $time) {
          $query = [
            'date' => $date,
            'city' => $city,
            'time' => $time
          ];

          return $this->crawler($type, $query);
      }


      // Returner data som array eller json
      public function getData($type = 0){
        if(!$type) {
          return $this->data;
        } else {
          return json_encode($this->data);
        }
      }

      // Handler for typen afgange / ankomster
      private function typeHandler($type){
        if($type == "arrivals"){
          return $type = 'afgange';
        }
        if($type == "depatures"){
          return $type = 'ankomster';
        }
      }



      private function crawler($type, $query=[]){

        $type = $this->typeHandler($type);


        // Hvis der er ekstra data med som CITY, eller DATE

        $searchQuery = '';

        if(!empty($query)) {

          $searchQuery = '?q=';

            if(!empty($query['city'])){
              $city = $query['city'];
              $searchQuery .= $city;
            }
            if(!empty($query['date'])){
                $date = $query['date'];


                $newdate = new DateTime($date);
                $searchQuery .= '&date='.$newdate->format('d').'%20-%20'.$newdate->format('m').'%20-%20'.$newdate->format('Y');
              }
            if(!empty($query['time'])){
                $time = $query['time'];
                $searchQuery .= '&time='.$time;
              }
            }

        $crawler = $this->client->request('GET', 'https://www.cph.dk/flyinformation/'.$type.'/'.$searchQuery);

        $data = [];

         $crawler = $crawler->filter('.stylish-table__row--body')->each(function($node) use($data){
            $data['time'] = $node->filter('.flights__table__col--time > div > span')->text();
            $data['destination'] = $node->filter('.flights__table__col--destination > div > span')->text();
            $data['terminal'] = $node->filter('.flights__table__col--terminal > div > span')->text();
           return $data;
         });


         $this->data = $crawler;

         return true;
      }
}



?>
